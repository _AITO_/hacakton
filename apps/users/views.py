from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, DetailView
from django.views.generic.base import TemplateResponseMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from .forms import UserCreationModelForm
from django.views.generic import CreateView, DeleteView
from django.contrib.auth.decorators import login_required
User = get_user_model()


class UserRegistrationView(CreateView):
    form_class = UserCreationModelForm
    success_url = reverse_lazy('login')
    template_name = 'user/registration.html'


class IndexPageView(TemplateView):
    template_name = 'users/index.html'


# class CabinetView(DetailView):
#     model = User
#
#
# class CabinetView(LoginRequiredMixin, DetailView):
#     model = User
#
#     def get_object(self):
#         return User.objects.get(username=self.request.user.username)


class CabinetView(LoginRequiredMixin, DetailView):

    def get_object(self):
        return self.request.user


def secret_page(request):
    return render(request, 'users/secret_page'),

@login_required
def secret_page(request):
    return render(request, 'users/secret_page.html')

