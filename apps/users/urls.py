from django.urls import path
from .views import UserRegistrationView, IndexPageView, CabinetView, secret_page

app_name = 'users'

urlpatterns = [
    path('accounts/register/', UserRegistrationView.as_view(), name='register'),
    path('', IndexPageView.as_view(), name='index'),
    path('accounts/cabinet/', CabinetView.as_view(), name='cabinet'),
    path('secret-page/', secret_page, name='secret_page')
]
